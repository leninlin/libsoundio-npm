const addon = require('./build/Release/AudioAddon');
const fs = require('fs');

let started = false;
let fd, params, filenameGlobal;

var convertBlock = (incomingData) => { // incoming data is a UInt8Array
	let view = new DataView(incomingData.buffer);
    var outputData = new Float32Array(incomingData.length/4);
	for (let i = 0; i < incomingData.length/4; i++) {
		outputData[i] = view.getFloat32(i*4, true);
	}
    return outputData;
}

module.exports.start = async (filename) => {
	if (started) return params;
	started = false;
	let size = 0;
	if (filename) {
		filenameGlobal = filename;
	}

	if (filename) {
		fd = await new Promise((resolve, reject) => {
			fs.open(filename, 'w+', (err, fd) => err ? reject(err) : resolve(fd));
		});
	}

	let event = async (dataRaw) => {
		if (!started) return;
		if (filename) {
			await new Promise((resolve, reject) => {
				let d = Buffer.from(dataRaw);
				fs.write(fd, d, 0, d.length, 44 + size, err => err ? reject(err) : resolve());
			});
			size += dataRaw.length;
			await new Promise((resolve, reject) => {
				let buf = new Buffer.alloc(4);
				buf.writeUInt32LE(size + 36);
				fs.write(fd, buf, 0, buf.length, 4, err => err ? reject(err) : resolve());
			});
			await new Promise((resolve, reject) => {
				let buf = new Buffer.alloc(4);
				buf.writeUInt32LE(size);
				fs.write(fd, buf, 0, buf.length, 40, err => err ? reject(err) : resolve());
			});
		}
	}

	params = await new Promise((resolve) => {
		addon.start((simple_rate, channel_count) => {
			if (filename) {
				let wav = encodeWAV(new Float32Array(0), 3, simple_rate, channel_count, 32);
				fs.writeFile(fd, Buffer.from(wav), err => {});
			}
			resolve({simple_rate, channel_count});
			started = true;
		}, event);
	})

	return params;
}

module.exports.stop = async () => {
	addon.end();
	started = false;
	if (fd) fs.close(fd, err => {});
	fd = undefined;
	params = undefined;
}

module.exports.stopAndGetData = async () => {
	addon.end();
	started = false;

	await new Promise((resolve, reject) => {
		if (fd) fs.close(fd, err => err ? reject(err) : resolve());
		else resolve();
	});
	fd = undefined;

	let data = await new Promise((resolve, reject) => {
		fs.readFile(filenameGlobal, (err, data) => err ? reject(err) : resolve(data));
	});
	return data;
}

function encodeWAV (samples, format, sampleRate, numChannels, bitDepth) {
	var bytesPerSample = bitDepth / 8
	var blockAlign = numChannels * bytesPerSample

	var buffer = new ArrayBuffer(44 + samples.length * bytesPerSample)
	var view = new DataView(buffer)

	/* RIFF identifier */
	writeString(view, 0, 'RIFF')
	/* RIFF chunk length */
	view.setUint32(4, 36 + samples.length * bytesPerSample, true)
	/* RIFF type */
	writeString(view, 8, 'WAVE')
	/* format chunk identifier */
	writeString(view, 12, 'fmt ')
	/* format chunk length */
	view.setUint32(16, 16, true)
	/* sample format (raw) */
	view.setUint16(20, format, true)
	/* channel count */
	view.setUint16(22, numChannels, true)
	/* sample rate */
	view.setUint32(24, sampleRate, true)
	/* byte rate (sample rate * block align) */
	view.setUint32(28, sampleRate * blockAlign, true)
	/* block align (channel count * bytes per sample) */
	view.setUint16(32, blockAlign, true)
	/* bits per sample */
	view.setUint16(34, bitDepth, true)
	/* data chunk identifier */
	writeString(view, 36, 'data')
	/* data chunk length */
	view.setUint32(40, samples.length * bytesPerSample, true)
	if (format === 1) { // Raw PCM
	  floatTo16BitPCM(view, 44, samples)
	} else {
	  writeFloat32(view, 44, samples)
	}

	return buffer
  }

  function writeFloat32 (output, offset, input) {
	for (var i = 0; i < input.length; i++, offset += 4) {
	  output.setFloat32(offset, input[i], true)
	}
  }

  function floatTo16BitPCM (output, offset, input) {
	for (var i = 0; i < input.length; i++, offset += 2) {
	  var s = Math.max(-1, Math.min(1, input[i]))
	  output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true)
	}
  }

  function writeString (view, offset, string) {
	for (var i = 0; i < string.length; i++) {
	  view.setUint8(offset + i, string.charCodeAt(i))
	}
  }
