{
  "targets": [
    {
  	  "include_dirs" : [ "<!(node -e \"require('nan')\")" ],
      "target_name": "AudioAddon",
      "sources": [ "./src/module.cc" ],
      "include_dirs": [
        "./node_modules/nan", "../nan", "./lib", "./lib/mac"
      ],
      "libraries": [ "-lsoundio" ],
      "link_settings": {
        "libraries": [ "-L<(module_root_dir)/lib/mac" ]
      }
    }
  ]
}