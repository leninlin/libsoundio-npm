#include <nan.h>
#include <uv.h>

#include <soundio/soundio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <unistd.h>

using v8::Function;
using v8::String;
using v8::Value;
using v8::Number;
using v8::Local;
using v8::Object;
using Nan::AsyncResource;
using Nan::New;
using Nan::HandleScope;
using Nan::Callback;
using Nan::To;
using Nan::NewBuffer;
using Nan::CopyBuffer;
using Nan::MaybeLocal;

struct ArgumentsAsync {
    Callback *cb;
    Callback *params;
};

bool stopped = true;

struct RecordContext {
    struct SoundIoRingBuffer *ring_buffer;
};
static enum SoundIoFormat prioritized_formats[] = {
    SoundIoFormatFloat32NE,
    SoundIoFormatFloat32FE,
    SoundIoFormatS32NE,
    SoundIoFormatS32FE,
    SoundIoFormatS24NE,
    SoundIoFormatS24FE,
    SoundIoFormatS16NE,
    SoundIoFormatS16FE,
    SoundIoFormatFloat64NE,
    SoundIoFormatFloat64FE,
    SoundIoFormatU32NE,
    SoundIoFormatU32FE,
    SoundIoFormatU24NE,
    SoundIoFormatU24FE,
    SoundIoFormatU16NE,
    SoundIoFormatU16FE,
    SoundIoFormatS8,
    SoundIoFormatU8,
    SoundIoFormatInvalid,
};
static int prioritized_sample_rates[] = {
    48000,
    44100,
    96000,
    24000,
    0,
};

struct payload {
    Callback* func;
    char* data;
    int length;
};

struct payloadParams {
    Callback* func;
    int sample_rate;
    int channel_count;
};

static int min_int(int a, int b) {
    return (a < b) ? a : b;
}

void close_cb (uv_handle_t* handle) {
    free(handle);
};

void doCallbackParams(uv_async_t* handle, int status) {
    HandleScope scope;
    AsyncResource resource("audioModule:callback_data");
    const unsigned argc = 2;
    struct payloadParams* p = (struct payloadParams*)handle->data;
    Local<Value> argv[argc] = {
        New<Number>(p->sample_rate),
        New<Number>(p->channel_count)
    };
    (p->func)->Call(argc, argv, &resource);
    delete p;
};

void doCallback(uv_async_t* handle, int status) {
    HandleScope scope;
    AsyncResource resource("audioModule:callback_data");
    const unsigned argc = 1;
    struct payload* p = (struct payload*)handle->data;

    MaybeLocal<Object> data = CopyBuffer(p->data, p->length);
    Local<Value> argv[argc] = {
        data.ToLocalChecked()
    };
    (p->func)->Call(argc, argv, &resource);
    delete p;
};

static void read_callback(struct SoundIoInStream *instream, int frame_count_min, int frame_count_max) {
    struct RecordContext *rc = (struct RecordContext *)instream->userdata;
    struct SoundIoChannelArea *areas;
    int err;
    char *write_ptr = soundio_ring_buffer_write_ptr(rc->ring_buffer);
    int free_bytes = soundio_ring_buffer_free_count(rc->ring_buffer);
    int free_count = free_bytes / instream->bytes_per_frame;
    if (free_count < frame_count_min) {
        fprintf(stderr, "ring buffer overflow\n");
        exit(1);
    }
    int write_frames = min_int(free_count, frame_count_max);
    int frames_left = write_frames;
    for (;;) {
        int frame_count = frames_left;
        if ((err = soundio_instream_begin_read(instream, &areas, &frame_count))) {
            fprintf(stderr, "begin read error: %s", soundio_strerror(err));
            exit(1);
        }
        if (!frame_count)
            break;
        if (!areas) {
            // Due to an overflow there is a hole. Fill the ring buffer with
            // silence for the size of the hole.
            memset(write_ptr, 0, frame_count * instream->bytes_per_frame);
        } else {
            for (int frame = 0; frame < frame_count; frame += 1) {
                for (int ch = 0; ch < instream->layout.channel_count; ch += 1) {
                    memcpy(write_ptr, areas[ch].ptr, instream->bytes_per_sample);
                    areas[ch].ptr += areas[ch].step;
                    write_ptr += instream->bytes_per_sample;
                }
            }
        }
        if ((err = soundio_instream_end_read(instream))) {
            fprintf(stderr, "end read error: %s", soundio_strerror(err));
            exit(1);
        }
        frames_left -= frame_count;
        if (frames_left <= 0)
            break;
    }
    int advance_bytes = write_frames * instream->bytes_per_frame;
    soundio_ring_buffer_advance_write_ptr(rc->ring_buffer, advance_bytes);
}
static void overflow_callback(struct SoundIoInStream *instream) {
    static int count = 0;
    fprintf(stderr, "overflow %d\n", ++count);
}

static void print_channel_layout(const struct SoundIoChannelLayout *layout) {
    if (layout->name) {
        fprintf(stderr, "%s", layout->name);
    } else {
        fprintf(stderr, "%s", soundio_get_channel_name(layout->channels[0]));
        for (int i = 1; i < layout->channel_count; i += 1) {
            fprintf(stderr, ", %s", soundio_get_channel_name(layout->channels[i]));
        }
    }
}
static bool short_output = false;

static void print_device(struct SoundIoDevice *device, bool is_default) {
    const char *default_str = is_default ? " (default)" : "";
    const char *raw_str = device->is_raw ? " (raw)" : "";
    fprintf(stderr, "%s%s%s\n", device->name, default_str, raw_str);
    if (short_output)
        return;
    fprintf(stderr, "  id: %s\n", device->id);
    if (device->probe_error) {
        fprintf(stderr, "  probe error: %s\n", soundio_strerror(device->probe_error));
    } else {
        fprintf(stderr, "  channel layouts:\n");
        for (int i = 0; i < device->layout_count; i += 1) {
            fprintf(stderr, "    ");
            print_channel_layout(&device->layouts[i]);
            fprintf(stderr, "\n");
        }
        if (device->current_layout.channel_count > 0) {
            fprintf(stderr, "  current layout: ");
            print_channel_layout(&device->current_layout);
            fprintf(stderr, "\n");
        }
        fprintf(stderr, "  sample rates:\n");
        for (int i = 0; i < device->sample_rate_count; i += 1) {
            struct SoundIoSampleRateRange *range = &device->sample_rates[i];
            fprintf(stderr, "    %d - %d\n", range->min, range->max);
        }
        if (device->sample_rate_current)
            fprintf(stderr, "  current sample rate: %d\n", device->sample_rate_current);
        fprintf(stderr, "  formats: ");
        for (int i = 0; i < device->format_count; i += 1) {
            const char *comma = (i == device->format_count - 1) ? "" : ", ";
            fprintf(stderr, "%s%s", soundio_format_string(device->formats[i]), comma);
        }
        fprintf(stderr, "\n");
        if (device->current_format != SoundIoFormatInvalid)
            fprintf(stderr, "  current format: %s\n", soundio_format_string(device->current_format));
        fprintf(stderr, "  min software latency: %0.8f sec\n", device->software_latency_min);
        fprintf(stderr, "  max software latency: %0.8f sec\n", device->software_latency_max);
        if (device->software_latency_current != 0.0)
            fprintf(stderr, "  current software latency: %0.8f sec\n", device->software_latency_current);
    }
    fprintf(stderr, "\n");
}
static int list_devices(struct SoundIo *soundio) {
    int output_count = soundio_output_device_count(soundio);
    int input_count = soundio_input_device_count(soundio);
    int default_output = soundio_default_output_device_index(soundio);
    int default_input = soundio_default_input_device_index(soundio);
    fprintf(stderr, "--------Input Devices--------\n\n");
    for (int i = 0; i < input_count; i += 1) {
        struct SoundIoDevice *device = soundio_get_input_device(soundio, i);
        print_device(device, default_input == i);
        soundio_device_unref(device);
    }
    fprintf(stderr, "\n--------Output Devices--------\n\n");
    for (int i = 0; i < output_count; i += 1) {
        struct SoundIoDevice *device = soundio_get_output_device(soundio, i);
        print_device(device, default_output == i);
        soundio_device_unref(device);
    }
    fprintf(stderr, "\n%d devices found\n", input_count + output_count);
    return 0;
}

void RecordAudio(void* argsAsync) {
    uv_async_t* handle = (uv_async_t*)malloc(sizeof(uv_async_t));
    uv_async_t* handleParams = (uv_async_t*)malloc(sizeof(uv_async_t));
    uv_async_init(uv_default_loop(), handle, (uv_async_cb) doCallback);
    uv_async_init(uv_default_loop(), handleParams, (uv_async_cb) doCallbackParams);
    stopped = false;

    struct ArgumentsAsync *args = (struct ArgumentsAsync *) argsAsync;
    Callback *cb = args->cb;
    Callback *params = args->params;

    enum SoundIoBackend backend = SoundIoBackendNone;
    char *device_id = NULL;
    bool is_raw = false;
    struct RecordContext rc;
    struct SoundIoDevice *selected_device = NULL;
    struct SoundIo *soundio = soundio_create();
    if (!soundio) {
        fprintf(stderr, "out of memory\n");
        return;
    }
    int err = (backend == SoundIoBackendNone) ?
        soundio_connect(soundio) : soundio_connect_backend(soundio, backend);
    if (err) {
        fprintf(stderr, "error connecting: %s", soundio_strerror(err));
        return;
    }
    soundio_flush_events(soundio);
    if (device_id) {
        for (int i = 0; i < soundio_input_device_count(soundio); i += 1) {
            struct SoundIoDevice *device = soundio_get_input_device(soundio, i);
            if (device->is_raw == is_raw && strcmp(device->id, device_id) == 0) {
                selected_device = device;
                break;
            }
            soundio_device_unref(device);
        }
        if (!selected_device) {
            fprintf(stderr, "Invalid device id: %s\n", device_id);
            return;
        }
    } else {
        int device_index = soundio_default_input_device_index(soundio);
        selected_device = soundio_get_input_device(soundio, device_index);
        if (!selected_device) {
            fprintf(stderr, "No input devices available.\n");
            return;
        }
    }
    fprintf(stderr, "Device: %s\n", selected_device->name);
    if (selected_device->probe_error) {
        fprintf(stderr, "Unable to probe device: %s\n", soundio_strerror(selected_device->probe_error));
        return;
    }
    soundio_device_sort_channel_layouts(selected_device);
    int sample_rate = 0;
    int *sample_rate_ptr;
    for (sample_rate_ptr = prioritized_sample_rates; *sample_rate_ptr; sample_rate_ptr += 1) {
        if (soundio_device_supports_sample_rate(selected_device, *sample_rate_ptr)) {
            sample_rate = *sample_rate_ptr;
            break;
        }
    }
    if (!sample_rate)
        sample_rate = selected_device->sample_rates[0].max;
    enum SoundIoFormat fmt = SoundIoFormatInvalid;
    enum SoundIoFormat *fmt_ptr;
    for (fmt_ptr = prioritized_formats; *fmt_ptr != SoundIoFormatInvalid; fmt_ptr += 1) {
        if (soundio_device_supports_format(selected_device, *fmt_ptr)) {
            fmt = *fmt_ptr;
            break;
        }
    }
    if (fmt == SoundIoFormatInvalid)
        fmt = selected_device->formats[0];
	struct SoundIoInStream *instream = soundio_instream_create(selected_device);
    if (!instream) {
        fprintf(stderr, "out of memory\n");
        return;
    }
    instream->format = fmt;
    instream->sample_rate = sample_rate;
    instream->read_callback = read_callback;
    instream->overflow_callback = overflow_callback;
    instream->userdata = &rc;
    if ((err = soundio_instream_open(instream))) {
        fprintf(stderr, "unable to open input stream: %s", soundio_strerror(err));
        return;
    }
    fprintf(stderr, "%s %dHz %s interleaved\n",
            instream->layout.name, sample_rate, soundio_format_string(fmt));
    const int ring_buffer_duration_seconds = 30;
    int capacity = ring_buffer_duration_seconds * instream->sample_rate * instream->bytes_per_frame;
    rc.ring_buffer = soundio_ring_buffer_create(soundio, capacity);
    if (!rc.ring_buffer) {
        fprintf(stderr, "out of memory\n");
        return;
    }
	if ((err = soundio_instream_start(instream))) {
        fprintf(stderr, "unable to start input device: %s", soundio_strerror(err));
        return;
    }

    struct payloadParams* pP = new payloadParams();
    pP->func = params;
    pP->sample_rate = sample_rate;
    pP->channel_count = instream->layout.channel_count;
    handleParams->data = (void *)pP;
    uv_async_send(handleParams);

    list_devices(soundio);

    while (1) {
        soundio_flush_events(soundio);
        usleep(100000);

        int fill_bytes = soundio_ring_buffer_fill_count(rc.ring_buffer);
        char *read_buf = soundio_ring_buffer_read_ptr(rc.ring_buffer);

        if (fill_bytes > 0) {
            struct payload *p = new payload();
            p->func = cb;
            p->data = read_buf;
            p->length = fill_bytes;
            handle->data = (void *)p;
            uv_async_send(handle);
        }

        soundio_ring_buffer_advance_read_ptr(rc.ring_buffer, fill_bytes);

        if (stopped == true) break;
    }

    soundio_instream_destroy(instream);
    soundio_device_unref(selected_device);
    soundio_destroy(soundio);

    uv_close((uv_handle_t*)handle, close_cb);
    uv_close((uv_handle_t*)handleParams, close_cb);
}

void StartAudio(const v8::FunctionCallbackInfo<v8::Value>& args) {
    HandleScope scope;

    Callback* params = new Callback(To<Function>(args[0]).ToLocalChecked());
    Callback* cb = new Callback(To<Function>(args[1]).ToLocalChecked());

    struct ArgumentsAsync *argsAsync = new ArgumentsAsync{
        cb, params
    };

    uv_thread_t id;
    uv_thread_create(&id, (uv_thread_cb) RecordAudio, argsAsync);
}

void EndAudio(const v8::FunctionCallbackInfo<v8::Value>& args) {
    stopped = true;
}

void Init (v8::Local<v8::Object> exports) {
    NODE_SET_METHOD(exports, "start", StartAudio);
    NODE_SET_METHOD(exports, "end", EndAudio);
}

NODE_MODULE(NODE_GYP_MODULE_NAME, Init)
